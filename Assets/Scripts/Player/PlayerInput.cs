﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

    private PlayerMovement mover;
    private PlayerRotation rotator;
    
	void Start () {
        mover = this.GetComponent<PlayerMovement>();
        rotator = this.GetComponent<PlayerRotation>();
    }

    void Update () {
        // Movement
        Vector3 displacement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (displacement != Vector3.zero)
        {
            mover.move(displacement);
        }

        // Rotation
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane viewPlane = new Plane(Vector3.up, new Vector3(0,1,0));
        float rayDistance;
        if (viewPlane.Raycast(mouseRay, out rayDistance))
        {
            Vector3 intersectionPoint = mouseRay.GetPoint(rayDistance);
            Debug.DrawLine(mouseRay.origin, intersectionPoint, Color.green);
            rotator.lookAt(intersectionPoint);
        }

        if (Input.GetMouseButton(0)) {

        }
	}
}
