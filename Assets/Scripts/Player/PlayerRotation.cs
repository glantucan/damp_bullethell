﻿using UnityEngine;
using System.Collections;

public class PlayerRotation : MonoBehaviour {

	public void lookAt(Vector3 lookPos)
    {
        transform.LookAt(lookPos);
    }
}
