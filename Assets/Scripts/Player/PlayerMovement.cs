﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    private float vel;

	public void move(Vector3 displacement)
    {
        transform.position += displacement * vel * Time.deltaTime;
    }
}
