﻿using UnityEngine;
using System.Collections;

public class MachineGun : MonoBehaviour {

    [SerializeField]
    private GameObject curAmmoPrefab;
   
    
	void Start () {
        GameObject bullet = Instantiate(curAmmoPrefab);
        bullet.transform.position = transform.position + transform.forward;
        IProjectile curAmmoPojectile = bullet.GetComponent<IProjectile>();
        curAmmoPojectile.release(transform.forward * 5f);
	}
	
}
