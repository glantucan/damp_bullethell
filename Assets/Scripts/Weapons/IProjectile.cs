﻿using UnityEngine;
using System.Collections;

public interface IProjectile {
    void release(Vector3 velocity);
}
