﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour, IProjectile {

    private Vector3 velocity;
	public void release(Vector3 velocity)
    {
        this.velocity = velocity;
    }

    void Update()
    {
        transform.position += velocity * Time.deltaTime;
    }
}
